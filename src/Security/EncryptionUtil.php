<?php

/*
 * This file is part of ContaoExtensionHelperBundle.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * This project is provided in good faith and hope to be usable by anyone.
 *
 * @package     ContaoExtensionHelperBundle
 * @author      Jens Doberenz <WurzelGnOOm>
 * @copyright   jedoLabs 2019 <https://jedo-Labs.de>
 * @license     LGPL-3.0-or-later
 * @see         <https://gitlab.com/jedoLabs/>
 */

namespace JedoLabs\ContaoExtensionHelperBundle\Security;

use Contao\System;

class EncryptionUtil
{

    public function decryptData($data)
    {
        $encryptionMethod = 'AES-256-CBC';
        $secretHash = \System::getContainer()->getParameter('contao.encryption_key');

        $cipherText = base64_decode($data, true);

        $bytes = '';
        $last = '';

        while (\strlen($bytes) < 48) {
            $last = md5($last.$secretHash, true);
            $bytes .= $last;
        }

        $iv = substr($bytes, 32, 16);
        $decryptedMessage = openssl_decrypt($cipherText, $encryptionMethod, $secretHash, 0, $iv);

        return $decryptedMessage;
    }

    public function encryptData($data)
    {
        $encryptionMethod = 'AES-256-CBC';
        $secretHash = \System::getContainer()->getParameter('contao.encryption_key');

        $bytes = '';
        $last = '';

        while (\strlen($bytes) < 48) {
            $last = md5($last.$secretHash, true);
            $bytes .= $last;
        }

        $iv = substr($bytes, 32, 16);
        $encryptedMessage = openssl_encrypt($data, $encryptionMethod, $secretHash, 0, $iv);

        return base64_encode($encryptedMessage);
    }

}
